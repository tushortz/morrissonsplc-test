import json
from collections import defaultdict


def ctree():
    return defaultdict(ctree)


def build_leaf(data_dict, leaf):
    """ 
    Recursive function to build desired custom tree structure
    """
    children = []

    # add children node if the leaf has any children
    if len(leaf.keys()):
        children = [build_leaf(k, v) for k, v in leaf.items()]

    data_dict = json.loads(data_dict)
    data_dict["children"] = children

    return data_dict


def add_row_to_node(row, tree):
    '''
    Transforms the current row into a dictionary and adds to existing node structure
    '''

    row = row[1:]

    # construct dynamic tree structure and
    # basically grouping csv values under their parents
    leaf = tree['{}']

    cid = 0
    size = len(row)

    # Check if we are within boundary of items we need
    # and also that the row doesn't have much more than we need
    while cid < size and size % 3 == 0:
        label = row[cid]

        data = {
            "label": label,
            "id": row[cid+1],
            "link": row[cid+2],
        }

        if label:
            leaf = leaf[json.dumps(data)]

        cid += 3


def convert_csv_to_json(stream):
    '''
    Read file and convert to json node
    '''

    tree = ctree()
    next(stream)

    for row in stream:
        row = row.strip().split(",")
        if not row[0]:
            continue

        add_row_to_node(row, tree)

    # building a custom tree structure
    result = {}
    for name, leaf in tree.items():
        result.update(build_leaf(name, leaf))

    result = result["children"]
    return result
