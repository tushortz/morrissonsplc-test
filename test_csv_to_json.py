import json
import pytest
from csv_to_json import add_row_to_node, build_leaf, ctree


def test_add_row_to_node():
    row1 = "https://base.url,label,id,link".split(",")
    row2 = "https://base.url,label,id,link,label2,id2,link2".split(",")
    row3 = "https://base.url,label,id,link,label2,id2,link2,label3,id3,link3".split(",")

    node = ctree()
    assert node["{}"] == {}

    add_row_to_node(row1, node)
    add_row_to_node(row2, node)
    add_row_to_node(row3, node)

    rows = set(row1[1:] + row2[1:] + row3[1:])
    
    for row in rows:
        assert row in str(node)




def test_build_leaf():
    parent = {
        "link": "https://",
        "id": 1,
        "label": "foo"
    }

    parent = json.dumps(parent)

    actual = build_leaf(parent, {})
    expected = {
        'id': 1, 
        'label': 'foo', 
        'link': 'https://',
        'children': []
    }

    assert actual == expected