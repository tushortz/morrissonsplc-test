import json
import os
import unittest
from selenium.webdriver.common.by import By
from selenium import webdriver


url = "http://127.0.0.1:5555"


class PageTest(unittest.TestCase):
    def setUp(self):
        options = webdriver.ChromeOptions()
        options.add_argument("-log-level=4")
        options.add_argument('--headless')
        self.driver = webdriver.Chrome(options=options)
        self.driver.get(url)

    def test_index_page_load(self):
        assert self.driver.title == 'Index'

    def test_submit_button(self):
        file_input = self.driver.find_element(
            by=By.CSS_SELECTOR, value="input[type='file']"
        )

        file_input.send_keys(os.path.join(os.getcwd(), 'data.csv'))

        submit_btn = self.driver.find_element(
            by=By.CSS_SELECTOR, value="button[type='submit']"
        )

        submit_btn.click()
        assert "label" in self.driver.page_source

    def tearDown(self):
        self.driver.close()
