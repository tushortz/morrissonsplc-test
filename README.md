# File generator

The file generator tech test comes with a mock data CSV that represents one of the many types of data that we have to deal with at Morrisons.

The challenge is to consume and transform the CSV file in to a nested JSON file which will form a tree structure.

## Getting Started

Technology used

- python version >= 3.8
- flask
- docker

> You can get docker from the url: https://docs.docker.com/get-docker. Follow the instructions on how to set it up on your local machine.

### Justification of choice

- initially I wanted to use Django but seeing that this is a light-weight program, I decided to use flask instead

## Advanced Setup

To build the docker container to run and setup the application, run the following

```sh
docker build --tag csv-app .
```

## Running the script with Docker

```sh
docker run -p 5555:5555 csv-app
```

You can then navigate to the url http://127.0.0.1:5555

To stop the container, use the command below

```sh
docker stop csv-app
```

## Running end 2 end test

To run end 2 end test, you need to first install npm then you can install chrome driver using the command below

```sh
npm install -g chromedriver
```

Ensure the application is running then run the command for the e2e test.

```
pytest e2e/test.py
```
