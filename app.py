import codecs
import os

from flask import Flask, jsonify, render_template, request
from livereload import Server

from csv_to_json import convert_csv_to_json

app = Flask(__name__)


@app.route('/', methods=["GET", "POST"])
def index():
    if request.method == 'POST':
        file = request.files["file"]
        stream = codecs.iterdecode(file.stream, 'utf-8')
        data = convert_csv_to_json(stream)
        return jsonify(data)

    return render_template("index.html")


if __name__ == '__main__':
    server = Server(app.wsgi_app)
    DEBUG = int(os.getenv('DEBUG', '0'))
    server.serve(
        port=5555,
        debug=DEBUG,
    )
